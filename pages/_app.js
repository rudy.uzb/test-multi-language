import 'antd/dist/antd.css'
import '../styles/vars.css'
import '../styles/global.css'
import '../styles/example-styles.css';

import {
  ConfigProvider,
  Radio,
} from 'antd';

import moment from 'moment';
import 'moment/locale/zh-cn';

const enUS = require('antd/es/locale/en_US');
const zhCN = require('antd/es/locale/zh_CN');

moment.locale('en');

export default function MyApp({ Component, pageProps }) {
  const [locale, setLocale] = useState(enUs);

  const changeLocale = (e) => {
    const localeValue = e.target.value;
    setLocale({ locale: localeValue });
    if (!localeValue) {
      moment.locale('en');
    } else {
      moment.locale('zh-cn');
    }
  }
  return (
    <div>
      <div className="change-locale">
        <span style={{ marginRight: 16 }}>Change locale of components: </span>
        <Radio.Group value={locale} onChange={changeLocale}>
          <Radio.Button key="en" value={enUS}>
            English
          </Radio.Button>
          <Radio.Button key="cn" value={zhCN}>
            中文
          </Radio.Button>
        </Radio.Group>
      </div>
      <ConfigProvider locale={locale}>
        <Component {...pageProps} key={locale ? locale : 'en'} />
      </ConfigProvider>
    </div>
  )
}
